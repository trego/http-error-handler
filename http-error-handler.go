package httperrorhandler

import (
	"net/http"

	"github.com/go-chi/render"
)

// ErrorResponse represents error response
type ErrorResponse struct {
	HTTPStatusCode int    `json:"-"`
	Error          string `json:"error"`
}

// Render process the response before rendered
func (e *ErrorResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, e.HTTPStatusCode)
	return nil
}

// CreateNotFoundResponse creates 404 response
func CreateNotFoundResponse(message string) *ErrorResponse {
	if message == "" {
		message = "Resource not found"
	}

	return &ErrorResponse{
		HTTPStatusCode: http.StatusNotFound,
		Error:          message,
	}
}

// CreateBadRequestResponse creates 400 response
func CreateBadRequestResponse(message string) *ErrorResponse {
	if message == "" {
		message = "Bad request"
	}

	return &ErrorResponse{
		HTTPStatusCode: http.StatusBadRequest,
		Error:          message,
	}
}

// CreateUnauthorizedResponse creates 401 response
func CreateUnauthorizedResponse(message string) *ErrorResponse {
	if message == "" {
		message = "Unauthorized"
	}

	return &ErrorResponse{
		HTTPStatusCode: http.StatusUnauthorized,
		Error:          message,
	}
}

// CreateUnprocessableEntityResponse creates 422 response
func CreateUnprocessableEntityResponse(message string) *ErrorResponse {
	if message == "" {
		message = "Unprocessable entity"
	}

	return &ErrorResponse{
		HTTPStatusCode: http.StatusUnprocessableEntity,
		Error:          message,
	}
}

// CreateInternalServerErrorResponse creates 500 response
func CreateInternalServerErrorResponse(message string) *ErrorResponse {
	if message == "" {
		message = "Internal server error"
	}

	return &ErrorResponse{
		HTTPStatusCode: http.StatusInternalServerError,
		Error:          message,
	}
}
